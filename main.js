const gameArea = document.getElementById("game");
const scoreElement = document.getElementById("score-value");
let score = 0;
let direction = { x: 0, y: 0 };
let snake = [{ x: 10, y: 10 }];
let food = null;

function createBlock(x, y, color) {
  const block = document.createElement("div");
  block.style.background = color;
  block.style.gridRowStart = y;
  block.style.gridColumnStart = x;
  block.classList.add("w-1", "h-1", "absolute");
  return block;
}

function updateGame() {
  // Update snake position
  const head = { ...snake[0] }; // Copy head
  head.x += direction.x;
  head.y += direction.y;
  snake.unshift(head); // Add new head to snake

  // Check for game over
  if (
    head.x < 1 ||
    head.y < 1 ||
    head.x > 16 ||
    head.y > 16 ||
    snake.some(
      (block, index) => index !== 0 && block.x === head.x && block.y === head.y,
    )
  ) {
    snake = [{ x: 10, y: 10 }];
    direction = { x: 0, y: 0 };
    score = 0;
  }

  // Check for food collision
  if (food && food.x === head.x && food.y === head.y) {
    score++;
    food = null;
  } else {
    snake.pop(); // Remove tail
  }

  // Generate new food
  if (!food) {
    food = {
      x: Math.floor(Math.random() * 16) + 1,
      y: Math.floor(Math.random() * 16) + 1,
    };
  }

  // Clear game area
  while (gameArea.firstChild) {
    gameArea.firstChild.remove();
  }

  // Draw snake
  for (const block of snake) {
    gameArea.appendChild(createBlock(block.x, block.y, "green"));
  }

  // Draw food
  gameArea.appendChild(createBlock(food.x, food.y, "red"));

  // Update score
  scoreElement.textContent = score;

  // Schedule next update
  setTimeout(updateGame, 100);
}

// Handle keyboard input
window.addEventListener("keydown", (e) => {
  console.log(`Key pressed: ${e.key}`);
  switch (e.key) {
    case "ArrowUp":
      console.log("Moving up");
      direction = { x: 0, y: -1 };
      break;
    case "ArrowDown":
      console.log("Moving down");
      direction = { x: 0, y: 1 };
      break;
    case "ArrowLeft":
      console.log("Moving left");
      direction = { x: -1, y: 0 };
      break;
    case "ArrowRight":
      console.log("Moving right");
      direction = { x: 1, y: 0 };
      break;
  }
});

// Start game
updateGame();
